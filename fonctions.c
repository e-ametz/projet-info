#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "fonctions.h"


/* Auteur : Antoine Metz */
/* Date :  18/06 */
/* Résumé : Affichage et préparation du jeu, fait jouer tous les joueurs sur le tableau de line (comprend tirage des cartes et placement de celles-ci selon coup spécial ou non) */
/* Entrée(s) : Tableau de joueurs, tableau de ligne et le tableau tabcartes qui contient toutes les cartes du jeu */
/* Sortie(s) :  Effectuer un tour de jeu du 6 qui Prend  */
void jouer(joueur* tabjoueurs,ligne* lignes, int nb,carte* tabcartes){
    int k;
    k=0;
    printf("\n|\n|\n");
    afftabjeu(lignes);
    printf("|\n|\n \n");
    for(int i=0;i<10;i++){
        selectioncarte(tabjoueurs,nb,k);
        k=(k%10)+1;
        tabjoueurs=ordredepassage(tabjoueurs,nb);
        for(int j=0;j<nb;j++){
            printf("tour de: %s ayant selectionné la carte %d \n",tabjoueurs[j].nom,tabjoueurs[j].jeu[tabjoueurs[j].id_carte-1].valeur);
            placercarte(tabjoueurs[j],lignes,tabcartes);
            triselection(tabjoueurs[j].jeu,10);
            printf("\n|\n|\n");
            afftabjeu(lignes);
            printf("|\n|\n \n");
        }
    }
    vide(tabcartes,lignes);
    for(int k=0;k<nb;k++){
        distribution(tabcartes,tabjoueurs[k].jeu,10);
    }
}

/* Auteur : Louise MAROLLEAU */
/* Date :  18/06/23 */
/* Résumé : fonction qui vérifie si un joueur à dépasser le seuil de 66 tetes */
/* Entrée(s) : prend en entrée le tableau de joueurs ainsi que sa taille en entier*/
/* Sortie(s) : retourne 1 si un joueur à dépasser 66, retourne 0 sinon  */
int veriftete(joueur* tabjoueurs,int nb){
    for(int i=0;i<nb;i++){
        if(*tabjoueurs[i].nb_tete>66){
            return 1;
        }
    }
    return 0;
}





/* Auteur : Berard Thierry */
/* Date :  18/06/23 */
/* Résumé :  Fonction qui demande le nombre de joueurs humain et retourne ce nombre */
/* Entrée(s) : Prend aucun paramètre */
/* Sortie(s) :  Retourne un entier, le nombre de joueurs humains */
int defnbjoueurs(){
    int nbhumains;
    printf("Combien de joueurs êtes-vous ? \n");
    scanf("%d",&nbhumains);
    return(nbhumains);
}







/* Auteur : Berard Thierry */
/* Date :  18/06/23 */
/* Résumé :  Fonction qui demande le nombre de bots et retourne ce nombre */
/* Entrée(s) : Prend aucun paramètre */
/* Sortie(s) :  Retourne un entier, le nombre de bots */
int defnbbots(){
    printf("Souhaitez vous intégrer des bots à la partie , (y/n) ? \n");
    char boolbot;
    scanf(" %c",&boolbot);
    if (boolbot=='y'){
        int nbbots;
        printf("Combien de bots voulez-vous ajouter ? \n");
        scanf("%d",&nbbots);
        return(nbbots);
    }
    else if (boolbot=='n'){
        printf("0 bots seront intégrés à la partie. \n");
        return(0);
    }
    printf("Erreur de saisie, 0 bots seront intégrées à la partie \n");
    return(0);
}




/* Auteur : Berard Thierry */
/* Date :  18/06/23 */
/* Résumé :  Fonction qui alloue de l'espace mémoire et créer le tableau de cartes dans lequel les joueurs piocherons leurs cartes selon le mode de jeu, si le mode de jeu est Normal ou Draft alors le jeu sera composer de 104 cartes sinon il sera composé de ((nombre total de joueurs * 10)+4) cartes */
/* Entrée(s) : Prend en paramètre deux entier, le nombre de joueurs total et la version du jeu  */
/* Sortie(s) :  Retourne le tableau de cartes tabcartes */
carte* allouertabcartes(int nb,int versiondujeu){
    carte* tab;
    tab=malloc(104*sizeof(carte));
    for (int i=0;i<104;i++){
        tab[i].valeur=i+1;
        tab[i].tete=teteboeuf(tab[i].valeur);   
    }
    if(versiondujeu==2){
        for(int k=((nb*10) + 4);k<104;k++){
            tab[k].valeur=404;
            tab[k].tete=404;
        }
    }
    return tab;
}





/* Auteur : Berard Thierry */
/* Date :  18/06/23 */
/* Résumé :  Procédure qui affiche un tableau de cartes en fonction de sa taille */
/* Entrée(s) : Prend en paramètre un tableau de cartes et sa taille en entier */
/* Sortie(s) :  Affichage */
void afftabcartes(carte* tab,int taille){
    for(int i=0;i<taille;i++){
        if(tab[i].valeur!=404){
            printf("%d,%d|",tab[i].valeur, tab[i].tete);
        }
        else{
            printf(" ");
        }
    }
    printf("\n");
}





/* Auteur : Berard Thierry */
/* Date :  18/06/23 */
/* Résumé :  Fonction qui initialise un tableau de cartes en fonction de sa taille en allouant de l'espace mémoire et en mettant des cartes neutres, utilisé principalement pour le jeu des joueurs et les lignes du plateau  */
/* Entrée(s) : Prend en paramètre la taille du tableau de carte qu'il faut initialiser en entier */
/* Sortie(s) :  Retourne un tableau de cartes initialiser */
carte* inittabcarte(int taille){
    carte* tab;
    tab=malloc(taille*sizeof(carte));
    for (int i=0;i<taille;i++){
        tab[i].valeur=404;
        tab[i].tete=404;
    }
    return tab;
}



/* Auteur : Antoine Metz */
/* Date :   18/06 */
/* Résumé : Echange les cartes et leur champs de place */
/* Entrée(s) :  */
/* Sortie(s) :  Changement de position  */
void echanger(carte* tab,carte* tab2,int i, int j){
    int temp;
    temp=tab[i].valeur;
    tab[i].valeur=tab2[j].valeur;
    tab2[j].valeur=temp;

    temp=tab[i].tete;
    tab[i].tete=tab2[j].tete;
    tab2[j].tete=temp;
}




/* Auteur : Antoine Metz */
/* Date :   18/06 */
/* Résumé : Distribue les cartes de manières aléatoires entre 1 et 104, pas de doublon possibles */
/* Entrée(s) : La main du joueur, le tableau de cartes et le nombre de cartes à distribuer */
/* Sortie(s) : Ajoute des cartes dans la main du joueur */
void distribution(carte* tabcartes,carte* tab,int nombre){
    int j;
    srand(time(NULL));
    for(int i=0;i<nombre;i++){
        do{            
            j=rand() % (103 + 1);
        }while(tabcartes[j].valeur == 404);
        echanger(tab,tabcartes,i,j);
    }    
}



/**
 * \fn emptyBuffer()
 * \author Peio Loubière <peio.loubiere@cyu.fr>
 * \date  : Thu Nov 19 17:07:41 2020
 *
 * \version 0.1
 * \brief vide le buffer après une saisie
 */
void emptyBuffer(){
  char c;
  while (((c = getchar()) != '\n') && (c != EOF));
}




/* Auteur : Thieyrry Berrard */
/* Date :   18/06 */
/* Résumé : Distribution des cartes avec séléction, utilisé pour le mode de jeu 3 */
/* Entrée(s) : Prend en paramètre le tableau tabcartes qui comprend toutes les cartes du jeu, le tableau de joueurs et le nombre de joueurs total */
/* Sortie(s) : distribue les cartes aux choix pour chaque joueur  */
void distribution2(carte* tabcartes,joueur* tabjoueurs,int nombre){
    int j;
    for(int i=0;i<10;i++){
        for(int k=0;k<nombre;k++){
            do{
                if(tabjoueurs[k].bot==0){
                    printf("%s donnez la valeur de la carte que vous voulez : ",tabjoueurs[k].nom);            
                    scanf("%d",&j);
                    emptyBuffer();
                }
                else{
                    j=rand() % (103 + 1);   
                }
            }while(tabcartes[j-1].valeur == 404);
            printf("%s a sélectionner la carte : %d \n",tabjoueurs[k].nom,tabcartes[j-1].valeur);
            echanger(tabjoueurs[k].jeu,tabcartes,i,j-1);
        }
    }    
}








/* Auteur : Antoine Metz */
/* Date :   18/06 */
/* Résumé : Effectue un tri par séléction sur le tableau en paramètre */
/* Entrée(s) : Tableau de carte, et la taille du tableau */
/* Sortie(s) :  Tri le tableau tab du plus petit au plus grand  */
void triselection(carte* tab,int taille){
    int min;
    for(int i=0;i<taille;i++){
        min=i;
        for(int j=i+1;j<taille;j++){
            if((tab[j].valeur < tab[min].valeur) && (tab[j].valeur != 404)){
                min=j;
            }
        }
        echanger(tab,tab,i,min);
    }
}


/* Auteur : Antoine Metz*/
/* Date :   18/06 */
/* Résumé : Initialise les différents champ d'une structure joueur */
/* Entrée(s) : Tableau de cartes, le nombre de bots et de joueurs humain et la version du jeu */
/* Sortie(s) : Initialisation du nom et carte des joueurs  */
joueur* creerjoueurs(carte* tabcartes, int taille,int nbjoueurs,int nbbots,int versiondujeu){
    joueur* tab;
    tab=malloc(taille*sizeof(joueur));
    if(versiondujeu==3){
        for(int q=0;q<nbjoueurs;q++){
            printf("entrez le nom du joueur: ");
            scanf("%s",tab[q].nom);
            tab[q].jeu=inittabcarte(10);
            tab[q].nb_tete=malloc(sizeof(int));
            tab[q].bot=0;
        } 
        for(int j=nbjoueurs; j<nbjoueurs+nbbots ; j++){ 
            printf("Création de bot %d : \n", j);
            sprintf(tab[j].nom,"Bot[%d]",j-nbjoueurs+1);
            tab[j].jeu=inittabcarte(10);
            tab[j].nb_tete=malloc(sizeof(int));
            tab[j].bot=1;
        }
        distribution2(tabcartes,tab,nbjoueurs+nbbots);
        for(int h=0;h<nbjoueurs+nbbots;h++){
            triselection(tab[h].jeu,10);
        } 
        
    }
    else{
        for(int i=0;i<nbjoueurs;i++){
            printf("entrez le nom du joueur: ");
            scanf("%s",tab[i].nom);
            tab[i].jeu=inittabcarte(10);
            distribution(tabcartes,tab[i].jeu,10);
            triselection(tab[i].jeu,10);
            tab[i].nb_tete=malloc(sizeof(int));
            tab[i].bot=0;
        }  
        for(int j=nbjoueurs; j<nbjoueurs+nbbots ; j++){ 
            printf("Création de bot %d : \n", j);
            sprintf(tab[j].nom,"Bot[%d]",j-nbjoueurs+1);
            tab[j].jeu=inittabcarte(10);
            distribution(tabcartes,tab[j].jeu,10);
            triselection(tab[j].jeu,10);
            tab[j].nb_tete=malloc(sizeof(int));
            tab[j].bot=1;
        }
    }
    
    

    return tab;
}

/* Auteur : Antoine Metz */
/* Date :   18/06 */
/* Résumé : Affiche le titre du projet en ascii */
/* Entrée(s) : X */
/* Sortie(s) :  Affichage  */
void affichage() {
    printf("  ██████╗ ██████╗  ██████╗      ██╗███████╗████████╗    ██████╗  ██╗\n"
           " ██╔══██╗██╔══██╗██╔═══██╗     ██║██╔════╝╚══██╔══╝    ██╔══██╗███║\n"
           " ██████╔╝██████╔╝██║   ██║     ██║█████╗     ██║       ██████╔╝╚██║\n"
           " ██╔═══╝ ██╔══██╗██║   ██║██   ██║██╔══╝     ██║       ██╔═══╝  ██║\n"
           " ██║     ██║  ██║╚██████╔╝╚█████╔╝███████╗   ██║       ██║      ██║\n"
           " ╚═╝     ╚═╝  ╚═╝ ╚═════╝  ╚════╝ ╚══════╝   ╚═╝       ╚═╝      ╚═╝\n"
           "                                                              \n"
           "  ██████╗          ██████╗ ██╗   ██╗██╗    ██████╗ ██████╗ ███████╗███╗   ██╗██████╗\n"
           " ██╔════╝         ██╔═══██╗██║   ██║██║    ██╔══██╗██╔══██╗██╔════╝████╗  ██║██╔══██╗\n"
           " ███████╗         ██║   ██║██║   ██║██║    ██████╔╝██████╔╝█████╗  ██╔██╗ ██║██║  ██║\n"
           " ██╔═══██╗        ██║▄▄ ██║██║   ██║██║    ██╔═══╝ ██╔══██╗██╔══╝  ██║╚██╗██║██║  ██║\n"
           " ╚██████╔╝        ╚██████╔╝╚██████╔╝██║    ██║     ██║  ██║███████╗██║ ╚████║██████╔╝\n"
           "  ╚═════╝          ╚══▀▀═╝  ╚═════╝ ╚═╝    ╚═╝     ╚═╝  ╚═╝╚══════╝╚═╝  ╚═══╝╚═════╝\n");
}




/* Auteur : Antoine Metz */
/* Date :   18/06 */
/* Résumé : Affiche le nom du joueur et sa main */
/* Entrée(s) : Le joueur et le nb de cartes qu'il possède */
/* Sortie(s) :  La main du joueur  */

void afftabjeujoueur(joueur* tab,int taille){
    for(int i=0;i<taille;i++){
        printf("%s : ",tab[i].nom);
        afftabcartes(tab[i].jeu,10);
    }
}





/* Auteur : Nathan Poireault */
/* Date :  18/06/23  */
/* Résumé : Procédure qui permet d'afficher le plateau de jeu */
/* Entrée(s) : Prend en entrée le plateau de jeu */
/* Sortie(s) : Affiche le plateau de jeu */
void afftabjeu(ligne*l){
    for(int i=0;i<4;i++){
        afftabcartes(l[i].cartes,l[i].nb_carte);
    }
}




/* Auteur : Nathan Poireault */
/* Date :  18/06/23  */
/* Résumé : Procédure qui permet à chaque joueur de choisir la carte qu'il va poser dans sa main */
/* Entrée(s) : Prend en entrée le tableau de joueurs, le nombre de joueurs et le nombre de tours depuis le début de la manche */
/* Sortie(s) : Affiche la carte choisi par le joueur */
void selectioncarte(joueur* tab,int nb,int k){
    for(int i=0;i<nb;i++){
        if (tab[i].bot==0){
        printf("%s : ",tab[i].nom);
        afftabcartes(tab[i].jeu,10);
        do{
            printf("%s choisissez une carte de votre main entre 1 et %d  :",tab[i].nom,10-k);
            scanf("%d",&tab[i].id_carte);
            printf("\n");
        }while((tab[i].jeu[tab[i].id_carte-1].valeur == 404)|| tab[i].id_carte>10 || tab[i].id_carte<1);
        printf("le joueur %s a selectionné la carte: %d\n\n",tab[i].nom,tab[i].jeu[tab[i].id_carte-1].valeur);
        }
        else if (tab[i].bot==1){
            printf("%s : ",tab[i].nom);
            afftabcartes(tab[i].jeu,10);
            do{
            tab[i].id_carte=rand()%10+1;
            }while((tab[i].jeu[tab[i].id_carte-1].valeur == 404)|| tab[i].id_carte>10 || tab[i].id_carte<1);
            printf("Le %s choisit la carte %d dans sa main \n",tab[i].nom,tab[i].jeu[tab[i].id_carte-1].valeur);
        }
        
    }
}



/* Auteur : Nathan Poireault */
/* Date :  18/06/23  */
/* Résumé : Procédure qui permet de placer les cartes choisi par les joueurs sur le tableau de jeu */
/* Entrée(s) : Prend en entrée un joueur, le plateau de jeu et le tableau contenant toutes les cartes du jeu  */
/* Sortie(s) : Modifie le tableau de jeu */
void placercarte(joueur j,ligne* l,carte* tabcartes){
    int x,x1,x2,x3,x4,X;
    x=j.jeu[j.id_carte-1].valeur;

    x1=l[0].cartes[l[0].nb_carte-1].valeur;
    x2=l[1].cartes[l[1].nb_carte-1].valeur;
    x3=l[2].cartes[l[2].nb_carte-1].valeur;
    x4=l[3].cartes[l[3].nb_carte-1].valeur;


    if((x-x1 < 0)&&(x-x2 < 0)&&(x-x3 < 0)&&(x-x4 < 0)){
        coupspecial(j,l,tabcartes);
    }
    else{
        X=min(min(x-x1,x-x2),min(x-x3,x-x4));
        if(X==x-x1){
            if(l[0].nb_carte==5){
                videligne(tabcartes,j,l,1);
                echanger(l[0].cartes,j.jeu,l[0].nb_carte,j.id_carte-1);
                l[0].nb_carte+=1;
            }
            else{
                echanger(j.jeu,l[0].cartes,j.id_carte-1,l[0].nb_carte);
                l[0].nb_carte+=1;
            }
        }
        if(X==x-x2){
            if(l[1].nb_carte==5){
                videligne(tabcartes,j,l,2);
                echanger(l[1].cartes,j.jeu,l[1].nb_carte,j.id_carte-1);
                l[1].nb_carte+=1;
            }
            else{
                echanger(j.jeu,l[1].cartes,j.id_carte-1,l[1].nb_carte);
                l[1].nb_carte+=1;
            }
        }
        if(X==x-x3){
            if(l[2].nb_carte==5){
                videligne(tabcartes,j,l,3);
                echanger(l[2].cartes,j.jeu,l[2].nb_carte,j.id_carte-1);
                l[2].nb_carte+=1;
            }
            else{
                echanger(j.jeu,l[2].cartes,j.id_carte-1,l[2].nb_carte);
                l[2].nb_carte+=1;
            }
        }
        if(X==x-x4){
            if(l[3].nb_carte==5){
                videligne(tabcartes,j,l,4);
                echanger(l[3].cartes,j.jeu,l[3].nb_carte,j.id_carte-1);
                l[3].nb_carte+=1;
            }
            else{
                echanger(j.jeu,l[3].cartes,j.id_carte-1,l[3].nb_carte);
                l[3].nb_carte+=1;
            }
        }
        
    }
}






/* Auteur : Nathan Poireault */
/* Date :  18/06/23  */
/* Résumé : Procédure qui permet au joueur de récupérer la ligne souhaitée quand se carte ne peut être directement placée sur le plateau */
/* Entrée(s) : Prend en entrée un joueur, le plateau de jeu et le tableau contenant toutes les cartes du jeu */
/* Sortie(s) : Modifie le tableau de jeu en ramassant une ligne et en la remplaceant par la carte du joueur  */
void coupspecial(joueur j,ligne* l,carte* tabcartes){
    int nb;
    if (j.bot==0){
        do{
            printf("c'est le coup spécial, choisissez la ligne que vous voulez récupérer entre 1 et 4 \n");
            scanf("%d",&nb);
        }while ((nb<1) || (nb>4));
        videligne(tabcartes,j,l,nb);
        echanger(l[nb-1].cartes,j.jeu,l[nb-1].nb_carte,j.id_carte-1);
        l[nb-1].nb_carte+=1;
    }
    else if (j.bot==1){
        printf("Le %s a déclenché le coup spécial \n",j.nom);
        nb=rand()%4 +1;
        printf("Le %s a récupéré la ligne numéro %d \n",j.nom,nb);
        videligne(tabcartes,j,l,nb);
        echanger(l[nb-1].cartes,j.jeu,l[nb-1].nb_carte,j.id_carte-1);
        l[nb-1].nb_carte+=1;
    }
}


/* Auteur : Nathan Poireault */
/* Date :  18/06/23  */
/* Résumé : Procédure qui permet de vider une ligne et de changer la carte avec celle du joueur */
/* Entrée(s) : Prend en entrée un joueur, le plateau de jeu et le numéro de la ligne de 1 à 4 */
/* Sortie(s) : Change le tableau de jeu et le nombre de tête que le joueur a au total  */
void videligne(carte* tabcartes,joueur j,ligne* l,int nbligne){
    for(int i=0;i<l[nbligne-1].nb_carte;i++){
        *j.nb_tete+=l[nbligne-1].cartes[i].tete;
        echanger(tabcartes,l[nbligne-1].cartes,l[nbligne-1].cartes[i].valeur-1,i);
    }
    l[nbligne-1].nb_carte=0;
    printf("le joueur : %s a au total %d tete \n",j.nom,*j.nb_tete);
}






/* Auteur : Louise MAROLLEAU */
/* Date :  18/06/23 */
/* Résumé : procédure qui remet à 0 chaque ligne après chaque manche elles seront alors remplis de carte neutre (404) */
/* Entrée(s) : prend en entrée le tableau tabcartes qui possède les cartes du jeu et les lignes du plateau */
/* Sortie(s) :  vide la ligne dans le plateau de jeu */
void vide(carte* tabcartes,ligne* l){
    for(int j=0;j<4;j++){
        for(int i=0;i<l[j].nb_carte;i++){
            echanger(tabcartes,l[j].cartes,l[j].cartes[i].valeur-1,i);
        }
        l[j].nb_carte=0;
    }
}





/* Auteur : Louise MAROLLEAU */
/* Date :  18/06/23 */
/* Résumé : fonction qui vérifie quelle est la carte la plus petite entre les deux */
/* Entrée(s) : prend en entrée les deux valeurs des cartes en entier */
/* Sortie(s) :  retourne un entier : la valeur de la carte la plus petite  */
int min(int a, int b){
    if(0<a && 0<b){
        if(a<b){
            return a;
        }
        else{
            return b;
        }
    }
    else{
        if(a<0){
            return b;
        }
        else{
            return a;
        }
    }
}



/* Auteur : Louise MAROLLEAU */
/* Date :  18/06/23 */
/* Résumé : fonction d'allocation de mémoire pour les lignes du plateau, elle remplit les lignes de carte neutre (404) */
/* Entrée(s) : prend en entrée le tableau tabcartes qui comprend toutes les cartes du jeu */
/* Sortie(s) :   retourne les lignes du plateau initialisées */
ligne* initligne(carte* tabcartes){
    ligne* l;
    l=malloc(4*sizeof(ligne));
    for(int i=0;i<4;i++){
        l[i].cartes=inittabcarte(6);
        distribution(tabcartes,l[i].cartes,1);
        l[i].nb_carte=1;
    }
    return l;
}





/* Auteur : Louise MAROLLEAU */
/* Date :  18/06/23 */
/* Résumé : fonctions qui détermine l'ordre de passage de chaque joueur selon la valeur de sa carte*/
/* Entrée(s) : prend en entrée le tableau de joueurs ainsi que sa taille en entier */
/* Sortie(s) : retourne le tableau de joueur trié dans l'ordre croissant dans lequel ils doivent jouer */
joueur* ordredepassage(joueur* tabjoueurs,int nb){
    carte* liste;
    joueur* tab;
    tab=malloc(nb*sizeof(joueur));
    liste=inittabcarte(nb);
    for(int k=0; k<nb;k++){
        liste[k].valeur=tabjoueurs[k].jeu[tabjoueurs[k].id_carte-1].valeur;
        liste[k].tete=tabjoueurs[k].jeu[tabjoueurs[k].id_carte-1].tete;
    }
    triselection(liste,nb);
    for(int i=0; i<nb;i++){
        for(int j=0;j<nb;j++){
            if(liste[i].valeur==tabjoueurs[j].jeu[tabjoueurs[j].id_carte-1].valeur){
                tab[i]=tabjoueurs[j];
            }
        }
    }
    free(liste);
    return tab;
    
}


/* Auteur : Berard Thierry */
/* Date :  18/06/23 */
/* Résumé :  Procédure qui tri le tableau de joueurs dans l'ordre croissant en fonction du nombre de tete  */
/* Entrée(s) : Prend en paramètre le tableau de joueurs et le nombre de joueurs total (comprend humain et bots) en entier*/
/* Sortie(s) :  Procédure donc pas de sortie mais tri le tableau de joueurs */
void classement(joueur* tabjoueurs, int nb){
    joueur temp;
    for(int i=0;i<nb-1;i++){
        for(int k=0;k<nb-i-1;k++){
            if(*tabjoueurs[k].nb_tete > *tabjoueurs[k+1].nb_tete){
                temp=tabjoueurs[k];
                tabjoueurs[k]=tabjoueurs[k+1];
                tabjoueurs[k+1]=temp;
            }
        }
    }
}





/* Auteur : Berard Thierry */
/* Date :  18/06/23 */
/* Résumé :  Procédure qui affiche dans l'ordre croissant (grace a la procédure classement()) le tableau de joueurs pour faire le classement*/
/* Entrée(s) :  Prend en paramètre le tableau de joueurs et le nombre de joueurs total (comprend humain et bots) en entier */
/* Sortie(s) :  Affichage */
void affclassement(joueur* tabjoueurs,int nb){
    classement(tabjoueurs,nb);
    printf("Le jeu est terminé\n\n\n");
    printf("Voici le classement :\n");
    for(int i=0;i<nb;i++){
        printf("%d-%s ayant - %d têtes\n",i+1,tabjoueurs[i].nom,*tabjoueurs[i].nb_tete);
    }
}



/* Auteur : Nathan Poireault */
/* Date :  18/06/23 */
/* Résumé : Fonction qui permet de définir les têtes de boeufs selon la valeur de la carte */
/* Entrée(s) : Prend en entrée la valeur de la carte */
/* Sortie(s) : Renvoie le nombre de têtes de boeufs */
int teteboeuf(int valeur){
	int tete;
	tete=1;
	if (valeur%10==0){
		tete=tete+2;
	}
	if (valeur%10==5){
		tete=tete+1;
	}
	if (valeur%11==0){
		tete=tete+4;
	}
	if (valeur==55){
		tete=7;
	}
	return tete;
}