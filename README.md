
# 6 Qui Prend



## A propos : 

Ce programme a été codé dans le cadre d'un projet de fin d'année de Pré-Ing 1 pour l'École Cy-Tech, cursus Math-Info Option Physique

Le programme a été entièrement réalisé en C. L’affichage est uniquement constitué de sorties textuelles dans le terminal via des printf.

## Installation

Téléchargez les 4 fichiers contenu dans la branche main.

Enregistrez-les dans la même destination, les fichiers ne doivent pas se situer dans des chemins différents.

![Logo](https://i.postimg.cc/hvksBx7h/Screenshot-603.png)

La compilation est laissée à l'utilisateur ; Celle-ci s'effectuera à l'aide d'un fichier Makefile.

Sous Linux ouvrez le terminal et rentrez la ligne de code suivante dans le dossier où vous avez téléchargé les fichiers.
```bash
   make
```
L'éxécutable apparaitra alors dans le dossier.

![Logo](https://i.postimg.cc/cLwx0NPJ/Screenshot-604.png)

## Utilisation

Éxécutez la commande suivante dans le terminal pour lancer le programme : 

```bash
   ./6quiprend
```
Suivez alors les instructions données par le programme dans le terminal, respectez les critères de saisies.

## Auteurs 

- Metz Antoine
- Berrard Thierry
- Poireault Nathan
- Marolleau Louise

## Contribution

Le projet n'est pas ouvert à la contribution étant donné qu'il a été codé dans le cadre d'une évaluation scolaire.