#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

typedef struct carte{
    int valeur;
    int tete;
}carte;

typedef struct ligne{
    carte* cartes;
    int nb_carte;
}ligne;

typedef struct joueur{
    char nom[50];
    carte* jeu;
    int id_carte;
    int* nb_tete;
    int bot;
}joueur;




void affichage();
carte* allouertabcartes(int nb,int versiondujeu);
int defnbjoueurs();
int defnbbots();
void afftabcartes(carte* tab,int taille);
carte* inittabcarte(int taille);
void echanger(carte* tab,carte* tab2,int i, int j);
void distribution(carte* tabcartes,carte* tab,int nombre);
void distribution2(carte* tabcartes,joueur* tabjoueurs,int nombre);
void triselection(carte* tab,int taille);
joueur* creerjoueurs(carte* tabcartes, int taille,int nbjoueurs,int nbbots,int versiondujeu);
void afftabjeujoueur(joueur* tab,int taille);
void afftabjeu(ligne*l);
void selectioncarte(joueur* tab,int nb,int k);
void placercarte(joueur j,ligne* l,carte* tabcartes);
void coupspecial(joueur j,ligne* l,carte* tabcartes);
void videligne(carte* tabcartes,joueur j,ligne* l,int nbligne);
void vide(carte* tabcartes,ligne* l);
int min(int a, int b);
ligne* initligne(carte* tabcartes);
joueur* ordredepassage(joueur* tabjoueurs,int nb);
void jouer(joueur* tabjoueurs,ligne* lignes, int nb,carte* tabcartes);
int veriftete(joueur* tabjoueurs,int nb);
void exit(int returnCode);
void classement(joueur* tabjoueurs, int nb);
void affclassement(joueur* tabjoueurs,int nb);
int teteboeuf(int valeur);