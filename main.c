#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "fonctions.h"



int main(){
//Affichage du titre
    affichage();
//initialisation
    int versiondujeu;
    int nb,k;
    int nbjoueurs;  
    int nbbots;

    joueur* tabjoueurs;
    carte* tabcartes;
    ligne* lignes;

    srand(time(NULL));

//code d'initialisation du jeu
    printf("A quelle version souhaitez-vous jouer ? 1,2,3 : (1: Normal, 2: Nombre définie, 3: Draft) \n");
    scanf("%d",&versiondujeu);
    
    nbjoueurs=defnbjoueurs();
    nbbots=defnbbots();
    nb=nbjoueurs+nbbots;

    if(nb>10){
        printf("Trop de joueurs dans la partie veuillez en selectionner au maximum 10\n");
        exit(EXIT_SUCCESS);
    }
    else if(nb<2){
        printf("Pas assez de joueurs dans la partie veuillez en selectionner plus\n");
        exit(EXIT_SUCCESS);
    }
    
    
    tabcartes=allouertabcartes(nb,versiondujeu);
    afftabcartes(tabcartes,104);

    tabjoueurs=creerjoueurs(tabcartes,nb,nbjoueurs,nbbots,versiondujeu);
    printf("Voici vos cartes : \n");
    afftabjeujoueur(tabjoueurs,nb);
    k=0;


 //corps principal, là où le jeu se lance
    do{
        lignes=initligne(tabcartes);
        printf("\nManche %d : \n",k+1);
        jouer(tabjoueurs,lignes,nb,tabcartes);
        k=k + 1;        
    }while(veriftete(tabjoueurs,nb)!=1);
    affclassement(tabjoueurs,nb);

    // Free des précédantes allocations mémorielles
    free(tabcartes);
    for(int m=0;m<nb;m++){
        free(tabjoueurs[m].jeu);
        free(tabjoueurs[m].nb_tete);
    
    
    }
    free(tabjoueurs);

    for (int n=0;n<4;n++){
        free(lignes[n].cartes);
    }
    free(lignes);
    return 0;
}